# Rocket

> Because static site generation isn't rocket science :rocket:

A completely unaware, non-smart static site generator. Create your content in markdown, get a completely static site with navigation with one command.