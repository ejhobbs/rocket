import sys
import os
from pathlib import Path, PurePath
from classes import Config, Directory, Page, Template


def create_if_not_present(path):
    if not Path.exists(path): Path.mkdir(path)

def src_dir_to_dest_dir(dest, cur):
    subdir = cur.parts[1:]
    path =  PurePath.joinpath(dest, *subdir)
    return path

def src_file_to_dest_file(dest, cur, ext = ".html"):
    raw_dest = src_dir_to_dest_dir(dest, cur)
    replaced = PurePath(raw_dest).with_suffix(ext)
    return Path(replaced)

if __name__ == "__main__":
    args = Config()

    in_dir = Path(args.get("src_dir"))
    out_dir = Path(args.get("dest_dir"))
    create_if_not_present(out_dir)
    template_file = Path(args.get("template"))
    if not template_file.exists():
        err = "ERR: Could not find template file: " + args.get("template")
        sys.exit(err)
    
    template = Template(template_file.read_text(), args.content_marker)
    for (cur_dir, subdirs, files) in os.walk(in_dir):
        cur = Path(cur_dir) 
        """
        First create all subdirectories
        """
        for dir in subdirs:
            """ gets full path, then substitutes first segment for out_dir """
            dest = src_dir_to_dest_dir(out_dir, cur.joinpath(dir))
            create_if_not_present(dest)

        """
        For each file in dir, create mirror in out with html version
        """
        for file in [Path(cur.joinpath(file)) for file in files]:
            page_partial = Page(file.name, file.read_text())
            dest = src_file_to_dest_file(out_dir, file)
            page_full = template.render_page(page_partial)
            dest.write_text(page_full)
