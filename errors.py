class UnknownConfigValueError(Exception):
    def __init__(self, message, available):
        msg = f"Unknown configuration value: {message}\n Available: {str(available)}"
        super().__init__(msg)

class InvalidTemplateError(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)