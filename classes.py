
import re
import argparse
from typing import List
from markdown2 import Markdown
from errors import UnknownConfigValueError, InvalidTemplateError

class Config:
    available_attrs = ["src_dir", "dest_dir", "static_dir", "template"]
    def __init__(self):
        super().__init__()
        parser = argparse.ArgumentParser(prog="Rocket")
        parser.add_argument("--src-dir", nargs=1, default="src", metavar="dir",
            help="Source directory to read markdown, default src")
        parser.add_argument("--dest-dir", nargs=1, default="out", metavar="dir",
            help="Destination for rendered HTML, default out. Will attempt creation if dir does not already exist")
        parser.add_argument("--static-dir", nargs=1, metavar="dir",
            help="Optional static content directory, copied to <dest-dir>/static if specified")
        parser.add_argument("--template", nargs=1, default="index.html", metavar="file",
            help="Template file for rendering each page, defaults to index.html")
        self.content_marker: str = "<% content %>"
        self.__args = parser.parse_args()
    
    def get(self, attr: str) -> str:
        if attr in self.available_attrs:
            return getattr(self.__args, attr)
        raise UnknownConfigValueError(attr, self.available_attrs)

class Page:
    __md_url_pattern: str = r"(\[[\s\S]+\]\s*)(\(\s*\S+\.)md\s*\)"
    __html_url_pattern: str = r"\1\2html)"
    __converter: Markdown = Markdown()

    def __init__(self, name: str, content: str):
        super().__init__()
        self.name = name
        self.__content = content

    def __fix_links(self) -> None:
        """Replace any links to other pages to a .html link (rather than .md)"""
        replaced = re.sub(Page.__md_url_pattern, Page.__html_url_pattern, self.__content)
        self.__content = replaced
    
    def toHtml(self) -> str:
        """Return content in HTML format, links to .md files replaced with .html"""
        self.__fix_links()
        return Page.__converter.convert(self.__content)

class Template:
    def __init__(self, content: str, marker:str = "<% content %>"):
        super().__init__()
        self.__content = content
        self.__marker = marker
    
    def render_page(self, page: Page) -> str:
        if self.__marker not in self.__content:
            raise InvalidTemplateError(f"Could not find content marker {self.__marker} in template file!")
        page_content = page.toHtml()
        this_content = re.sub(self.__marker, page_content, self.__content)
        return this_content

class Directory:
    def __init__(self, name: str, children: List['Directory'], pages: List['Page']):
        super().__init__()
        self.name = name
        self.children = children
        self.pages = pages
